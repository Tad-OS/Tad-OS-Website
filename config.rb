# Asset and directories
activate :relative_assets

set :base_url, '/middleman'
set :build_dir, 'public'
set :relative_links, true

# Ignore some things during build
ignore 'helpers/*'
ignore 'fonts/*/LICENSE.txt'
ignore 'docs/README.adoc'

# Per-page layout changes:
page 'docs/*', :layout => 'docs'
page 'tadman/*', :layout => 'docs'

# Files with no layout
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

## Middleman configuration
activate :deploy do |deploy|
   deploy.build_before = true
   deploy.deploy_method = :git
#   deploy.remote   = 'custom-remote' # remote name, default: origin
#   deploy.branch   = 'custom-branch' # default: gh-pages
#   deploy.strategy = :submodule
   commit_hash = `git log --pretty=format:'%h' -n 1`
   deploy.commit_message = "Automated commit for #{commit_hash} at \
                            #{Time.now.utc}"
end

# Reload the browser automatically whenever files change
configure :development do
  activate :livereload
end

# Helpers
helpers do
  # Return a nicely formatted page title
  def page_title (page_title, site_title)
    return "#{page_title} | #{site_title}"
  end
  # Get the time right now
  def get_time_message
    time_now = Time.now
    formatted_date = time_now.strftime("%Y/%m/%d")
    formatted_time = time_now.strftime("%I:%M:%S %p")
    time_message = "#{formatted_date} at #{formatted_time}"
    time_message
  end
  # Create a button with an image on it
  # a_link is a string of a link
  # a_message is what will be printed on the button in string form
  # an_image is a path to an image relative to the image_src directory
  # an_alt is the alt message for the image
  def make_button_with_image (a_link, a_message, an_image, an_alt)
    out_string = ''
    in_array = ["<a href='#{a_link}'>\n", "<button type='button'>",
                "<p class='button-text'>#{a_message}</p>",
                (image_tag an_image, :alt => an_alt, :class => 'button-img'),
                "</button>\n</a>"]
    in_array.each do |output|
      out_string << "#{output}\n"
    end
    out_string
  end
end

after_build do
  #FileUtils.cp('CNAME', './build')
end
