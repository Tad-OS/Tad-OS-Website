# A quick little helper to create a table of contents for each document in a
# directory. Not entirely sure if it works though.

module ContentHelpers
  # Walk through a directory and return it's contents
  def directory_walker(directory)
    contents = {}
    exist_dirs = Dir.glob("#{directory}/**/")

    exist_dirs.each do |child_dir|
      sub_dir = child_dir.chomp('/')
      contents[sub_dir] = []

      doc_glob = "#{sub_dir}/*.html.adoc"
      (Dir[doc_glob]).each do |squid|
        contents[sub_dir] << squid.gsub("#{sub_dir}/", '')
        return contents
      end
    end
  end

  # Reformat a string so it is only the title value
  def get_page_title(a_string)
    title_line = a_string.split(' ')
    title_line.shift
    return title_line.join(' ')
  end

  # Read a file and get the title value from it
  def find_page_title(a_path)
    File.open(a_path, 'r') do |a_file|
      a_file.each_line do |a_line|
        next unless a_line.include? 'title: '
        get_page_title(a_line)
        return nil if page_title.nil?
      end
    end
  end

  # Walk through a directory to find all files and see if they have a title
  # in them. If so, add them to the table of contents.
  def get_html_output(a_path)
    walked_output = directory_walker(a_path)
    walked_dirs = walked_output.keys

    walked_dirs.each do |a_key|
      root_dir = "#{a_path}/"

      if a_key.eql? a_path
        dir_title = 'Tad OS'
      else
        the_dir = a_key.gsub(root_dir, '').chomp('/')
        dir_title = the_dir.capitalize
      end

      puts "<h2>#{dir_title}</h2>"
      puts '<ul>'

      walked_output[a_key].each do |a_file|
        # file_path = "#{a_key}/#{a_file}"
        file_path = a_file
        title = find_page_title(file_path)
        puts "  <li><a href='#{file_path.sub('.adoc', '')}'>#{title}</a>"
      end
      puts '</ul>'
    end
  end
end
