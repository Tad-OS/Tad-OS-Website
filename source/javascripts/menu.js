// Show the menu!
function ShowMenu()
{
  var menuButton, menuList

  menuButton = document.getElementById('menu-button')
  menuList = document.getElementById('menu-window')
  menuList.classList.toggle('show')

  if (menuButton.innerHTML == '☰')
  {
    menuButton.innerHTML = '✕'
  }
  else
  {
    menuButton.innerHTML = '☰'
  }
}
